import copy
from argparse import ArgumentParser

import torch
import torch.nn as nn
from tqdm import tqdm
from clearml import Task

from src.data.make_dataset_stage_1 import prepare_train_dataset
from src.models.model import conv_net_model, device

if __name__ == "__main__":
    task = Task.init(
        project_name="SST Number recognition",
        task_name="Logs",
        output_uri=True,
        auto_connect_frameworks={"pytorch": ["model.pth"], "matplotlib": True},
    )
    parser = ArgumentParser()
    parser.add_argument(
        "--epochs",
        required=True,
        type=int,
        default=50,
        help="number of epochs when training",
    )
    parser.add_argument(
        "--save_path",
        required=True,
        type=str,
        default="model.pth",
        help="path to save weights",
    )
    args = parser.parse_args()

    LR = 1e-3

    loss = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(conv_net_model.parameters(), lr=LR)

    best_model_wts = None
    best_model_loss = 1e9

    train_dl, val_dl = prepare_train_dataset()

    for epoch in tqdm(range(args.epochs)):
        train_loss = 0
        val_loss = 0

        train_correct = 0
        val_correct = 0

        conv_net_model.train()
        for i, (X, y) in enumerate(train_dl):
            step_idx = epoch * len(train_dl) + i
            X_gpu = X.to(device)
            y_gpu = y.long().to(device)
            y_pred = conv_net_model(X_gpu)
            l_gpu = loss(y_pred, y_gpu)

            optimizer.zero_grad()
            l_gpu.backward()
            optimizer.step()

            l_gpu_item = l_gpu.cpu().item()
            train_loss += l_gpu_item * len(y)
            train_correct += (y_pred.argmax(dim=1) == y_gpu).float().sum()

        conv_net_model.eval()
        with torch.no_grad():
            for i, (X, y) in enumerate(val_dl):
                step_idx = epoch * len(val_dl) + i
                X_gpu = X.to(device)
                y_gpu = y.long().to(device)

                y_pred = conv_net_model(X_gpu)
                l_gpu = loss(y_pred, y_gpu)

                l_gpu_item = l_gpu.cpu().item()
                val_loss += l_gpu_item * len(y)
                val_correct += (y_pred.argmax(dim=1) == y_gpu).float().sum()

        avg_train_loss = train_loss / 13066  # len(train_dataset)
        train_acc = train_correct / 13066  # len(train_dataset)

        avg_val_loss = val_loss / 5600  # len(val_dataset)
        val_acc = val_correct / 5600  # len(val_dataset)

        if avg_val_loss < best_model_loss:
            best_model_loss = avg_val_loss
            best_model_wts = copy.deepcopy(conv_net_model.state_dict())

        print(
            f"epoch: {epoch} - train_loss: {avg_train_loss:.5f} - train_acc: {train_acc:.5f} "
            f"- val_loss: {avg_val_loss:.5f} - val_acc: {val_acc:.5f}"
        )
        task.update_output_model(model_path="model.pth")
    torch.save(conv_net_model.state_dict(), args.save_path)
