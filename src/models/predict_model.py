from argparse import ArgumentParser

import torch
from tqdm import tqdm

from src.data.make_dataset_stage_1 import prepare_test_dataset, CLASSES_NAMES
from src.models.model import conv_net_model, device

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument(
        "--weights",
        required=True,
        type=str,
        default="model.pth",
        help="path to model's weights",
    )
    args = parser.parse_args()

    conv_net_model.load_state_dict(torch.load(args.weights))
    conv_net_model.eval()

    test_dl = prepare_test_dataset()

    test_pred = []
    for x in tqdm(test_dl):
        output = conv_net_model(x.to(device)).cpu()
        class_ids = torch.argmax(output, dim=1).numpy()
        for cl_id in class_ids:
            print(f"Предсказание: {CLASSES_NAMES[cl_id]}")
