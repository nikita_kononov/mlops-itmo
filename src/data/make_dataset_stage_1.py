from pathlib import Path

import pandas as pd
import torch
import torch.nn.functional as F
import torchaudio
from torch.utils.data import DataLoader
from torch.utils.data.dataset import Dataset
from tqdm import tqdm
import yaml

with open("../config.yaml") as file:
    config = yaml.safe_load(file)

CLASSES_NAMES = config["classes_names"]
DATASET_TRAIN_DIR = Path(config["data_dir"], config["dataset_train_dir"], "train")
DATASET_TEST_DIR = Path(config["data_dir"], config["dataset_test_dir"], "test")
TRAIN_PREPROC_DIR = Path("tmp", "preproc", "train")
MAX_WIDTH, MAX_HEIGHT = config["max_width"], config["max_height"]
BATCH_SIZE = config["batch_size"]

assert DATASET_TRAIN_DIR.is_dir(), "Train dataset was not found."
assert DATASET_TEST_DIR.is_dir(), "Test dataset was not found."


class AudioDataset(Dataset):
    def __init__(self, dfdata, save_folder: Path):
        save_folder.mkdir(parents=True, exist_ok=True)

        self.df_audio = dfdata

        tensor_paths = []
        for index, row in tqdm(self.df_audio.iterrows(), desc="Формирование массивов"):
            waveform, _ = torchaudio.load(row["filepath"])
            spec = torchaudio.transforms.Spectrogram()(waveform)
            spec = F.pad(
                spec, [0, MAX_WIDTH - spec.size(2), 0, MAX_HEIGHT - spec.size(1)]
            )
            save_tensor_path = f"{save_folder}/{str(index).zfill(5)}.pt"
            torch.save(spec, save_tensor_path)
            tensor_paths.append(save_tensor_path)
        self.df_audio["tensor_path"] = tensor_paths

    def __getitem__(self, index):
        tensor_path = self.df_audio.iloc[index].tensor_path
        label = self.df_audio.iloc[index].label
        spec = torch.load(tensor_path)
        return spec, label

    def __len__(self):
        count = len(self.df_audio)
        return count


class TestAudioDataset(Dataset):
    def __init__(self, list_dir, max_width, max_height):
        self.list_audio = list_dir
        self.max_width = max_width
        self.max_height = max_height

    def __getitem__(self, index):
        waveform, _ = torchaudio.load(self.list_audio[index])
        spec = torchaudio.transforms.Spectrogram()(waveform)
        spec = F.pad(
            spec, [0, self.max_width - spec.size(2), 0, self.max_height - spec.size(1)]
        )
        return spec

    def __len__(self):
        count = len(self.list_audio)
        return count


def prepare_train_dataframe(train_path: Path) -> pd.DataFrame:
    df_data = {"number": [], "label": [], "filepath": []}

    for class_folder in train_path.iterdir():
        for file_path in class_folder.iterdir():
            df_data["number"].append(class_folder.stem)
            df_data["label"].append(CLASSES_NAMES.index(class_folder.stem))
            df_data["filepath"].append(file_path)

    dataframe = pd.DataFrame.from_dict(df_data)

    return dataframe


def prepare_train_dataset():
    print("Формирование списка файлов и создание таблицы")
    train_dataframe = prepare_train_dataframe(DATASET_TRAIN_DIR)

    print("Формирование и сохранение массивов")
    data_train = AudioDataset(train_dataframe, TRAIN_PREPROC_DIR)

    print("Разделение на train/val выборки")
    train_size = round(len(data_train) * 0.7)
    val_size = len(data_train) - train_size
    train_dataset, val_dataset = torch.utils.data.random_split(
        data_train, [train_size, val_size]
    )

    print("Создание экземпляров DataLoader для train/val выборок.")
    train_dl = DataLoader(train_dataset, batch_size=BATCH_SIZE, shuffle=True)
    val_dl = DataLoader(val_dataset, batch_size=BATCH_SIZE, shuffle=False)

    return train_dl, val_dl


def prepare_test_dataset():
    test_files = list(DATASET_TEST_DIR.glob("*"))
    test_dataset = TestAudioDataset(test_files, MAX_WIDTH, MAX_HEIGHT)

    test_dataloader = DataLoader(test_dataset, batch_size=BATCH_SIZE, shuffle=False)

    return test_dataloader
