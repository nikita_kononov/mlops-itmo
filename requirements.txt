pandas==1.2.4
torchaudio==0.11.0
torchsummary==1.5.1
tqdm==4.64.0
black==23.3.0
clearml==1.9.3
dvc==2.58.2